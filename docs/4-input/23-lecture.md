# 👓 Lecture de l'entrée

!!! cite "Rappel"
    Sur la première page `Hello World!`, on a évoqué que les programmes informatiques envoient des résultats à d'autres qui font de nouvelles taches avec. Une technique possible est d'écrire les résultats dans un fichier et le programme suivant lit ses paramètres dans le fichier.

!!! example "Exemple"
    Un premier programme pourrait avoir `0 1 10` en paramètres à lire.

    * Il lit ses trois paramètres
    * Il affiche le premier, puis le deuxième, et dix fois de suite affiche la somme des deux nombres affichés. Donc 12 nombres affichés.

    ```output
    0 1 1 2 3 5 8 13 21 34 55 89
    ```

    Un autre programme pourrait utiliser cette sortie comme son entrée à lui. Son rôle serait de fabriquer un tableau HTML dont le rendu serait :

    |$n=0$|$n=1$|$n=2$|$n=3$|$n=4$|$n=5$|$n=6$|$n=7$ |$n=8$ |$n=9$ |$n=10$|$n=11$|
    |:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|:---:|
    |$F(0)=0$|$F(1)=1$|$F(2)=1$|$F(3)=2$|$F(4)=3$|$F(5)=5$|$F(6)=8$|$F(7)=13$|$F(8)=21$|$F(9)=34$|$F(10)=55$|$F(11)=89$|

Un programme est souvent utilisé avec un système d'entrée-sortie et branché avec un autre.

!!! info "_Data scientist_"
    Parmi les nombreux métiers de l'informatique, il y a les scientifiques qui analysent les données d'expériences. Les appareils de mesure enregistrent des données qui sont transmises à des programmes. Des programmes qui sont écrits ou utilisés par des humains.

## La lecture avec Python

On peut lire :

- Du texte,
- c'est tout !

Enfin, **directement**, on ne peut lire que du texte.

=== "Exemple 1"

    ```email
    première ligne
    seconde ligne plus longue
    ```

    ```python
    ligne_1 = input()
    ligne_2 = input()

    print(ligne_2)
    print(ligne_1)
    ```

    ```output
    seconde ligne plus longue
    première ligne
    ```

    Le programme a seulement lu deux lignes, puis affiché à l'envers ces deux lignes.


=== "Exemple 2"

    ```email
    première ligne
    seconde ligne plus longue
    ```

    ```python
    ligne_1 = input()
    
    print(ligne_1)
    ```

    ```output
    première ligne
    ```

    Le programme n'a lu qu'une ligne, puis l'a affichée. C'est possible.


=== "Exemple 3"

    ```email
    première ligne
    ```

    ```python
    ligne_1 = input()
    ligne_2 = input()
    
    print(ligne_2)
    print(ligne_1)
    ```

    ```pycon
    Traceback (most recent call last):
    File "<stdin>", line 1, in <module>
    EOFError
    ```

    Le programme a pu lire la `ligne_1`, mais en essayant de lire `ligne_2` le curseur de lecture du fichier était à la fin du fichier. On a un message d'erreur `#!python EOFError` qui signifie : _End Of File Error_, **erreur de fin de fichier**.



!!! warning "Regardez bien ces **3** exemples"
    Avez-vous compris le problème de `#!python EOFError` ? Relisez encore les 3 exemples précédents !!!

    Il ne faut pas lire plus de lignes qu'il n'y a de disponibles ! C'est une erreur fréquente.

## Lecture de nombres

Pour lire un entier, on lit une ligne, on obtient du texte, et on demande à une fonction Python de convertir le texte en entier. Il s'agit de la fonction `int`. (pour _integer_)

Pour lire un nombre à virgule flottante, on lit une ligne, on obtient du texte, et on demande à une fonction Python de convertir le texte en nombre flottant. Il s'agit de la fonction `float`. (pour _floating point number_)


=== "Exemple 1"

    ```email
    42
    ```

    ```python
    ligne = input()
    nombre = int(ligne)

    print(nombre * 2)
    ```

    ```output
    84
    ```

    - `ligne` est du texte.
    - `nombre` est un **entier**, converti par `int`.
    - On peut faire des calculs mathématiques avec...

=== "Exemple 2"

    ```email
    2.718
    ```

    ```python
    ligne = input()
    nombre = float(ligne)

    print(nombre + 3.0)
    ```

    ```output
    5.718
    ```

    - `ligne` est du texte.
    - `nombre` est un **flottant**, converti par `float`.
    - On peut faire des calculs mathématiques avec...
    
=== "Exemple 3"

    ```email
    42
    ```

    ```python
    nombre = input()

    print(nombre * 2)
    ```

    ```output
    4242
    ```

    - `nombre` est du **texte**, ce n'est pas un nombre.
    - On **ne peut pas** faire des calculs mathématiques avec...
    - Mais, ici, Python a collé deux fois le texte, il n'y a pas d'erreur (enfin...).
    - `#!python print("bla" * 3)` aurait donné `blablabla`


!!! warning "Regardez bien ces **3** exemples"
    Avez-vous compris la fabrication de `blablabla` ? Regardez à nous ces trois exemples !!!

    Il faut convertir le texte en nombre si on veut faire des calculs.

    Il est possible d'écrire les deux étapes en une seule instruction.


    === "Version courte"

        ```python
        nombre = int(input())

        ...
        ```

    === "Version longue"

        ```python
        ligne = input()
        nombre = int(ligne)

        ...
        ```


!!! savoir "En résumé"
    1. Il faut lire dans le bon ordre les paramètres.
    2. Il faut les convertir correctement en fonction de l'énoncé.
    3. Il ne faut pas en lire plus qu'il n'y a de disponibles.

    Vous pouvez vous entrainer sur [cette page.](http://www.france-ioi.org/algo/course.php?idChapter=843&idCourse=2749){target=_blank}


!!! warning "Autre erreur possible"
    Si vous essayez de convertir du texte vide ou bien un texte qui ne donne pas un nombre comme `'coucou'`, alors vous aurez une erreur.

    ```email
    coucou
    ```

    ```python
    nombre = int(input())

    print(nombre * 2)
    ```

    ```pycon
    ValueError: invalid literal for int() with base 10: 'coucou'
    ```

    La fonction `int` ne peut pas convertir ce texte avec la base $10$.

