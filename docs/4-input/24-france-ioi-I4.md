# 👾 France-IOI (I-4)

Le niveau 1 de France-IOI se poursuit avec [Lecture de l'entrée](http://www.france-ioi.org/algo/chapter.php?idChapter=843){target=_blank}

## 1) Récoltes

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&idTask=1920) est de :

> Lire un entier et faire un calcul simple


!!! tip "Indices"
    1. Utiliser trois variables bien nommées.
    2. Mettre les unités en commentaires, c'est utile.

    2. Compléter le code

    ```python
    RENDEMENT = 23         # en kg / m²
    ??? = int(input())     # en m
    ??? = ...              # en ???
    ??? = ...              # en ???

    print(production)
    ```

## 2) Retraite spirituelle

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=3) est de :

> Lire un nombre et faire plusieurs calculs simples.

!!! tip "Indice"
    1. Revoir l'exercice précédent.
    2. Utiliser des noms de variables qui permettent de comprendre quel était l'énoncé sans avoir besoin de le lire.



## 3) Âge des petits-enfants

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=4) est de :

> Corriger un programme

!!! tip "Indice"
    Il suffit de faire un copier-coller et faire quelques petites modifications.


## 4) Encore des punitions

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=7) est de :

> Écrire autant de fois que demandé la phrase « Je dois suivre en cours ». 

!!! tip "Indice"
    Le programme devrait tenir en trois ou quatre lignes.


## 5) Graduation de thermomètres

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=8) est de :

> Afficher les nombres entre deux nombres lus en entrée.

!!! tip "Indices"
    Combien voulez-vous faire de tours de boucle ? Êtes-vous sûr ?


## 6) Jeu de calcul mental

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=10) est de :

> Afficher une certaine quantité de nombres à un jeu


!!! tip "Indices"
    1. Utiliser une variable `nb_tours` à lire sur l'entrée
    2. Utiliser une variable `nombre_affiche` à afficher en sortie
    3. Utiliser une variable `coeff_multiplicateur`

## 7) La Grande Braderie

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&idTask=1951) est de :

> Afficher des nombres en progression arithmétique

!!! tip "Indices"
    1. Combien voulez-vous faire de tours de boucle ? Êtes-vous sûr ?
    2. Utilisez les noms de variable
        - `position_depart`
        - `largeur_emplacement`
        - `nb_vendeurs`
        - `position` qui sera modifiée au cours du programme.

## 8) Bétail

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=13) est de :

> Faire la somme de 20 entiers lus.

!!! tip "Indice"
    Préparez une boucle pour une somme cumulée.


## 9) Socles pour statues

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=14) est de :

> Calculer un volume total

!!! tip "Indices"
    1. Combien voulez-vous faire de tours de boucle ? Êtes-vous sûr ?
    2. Utilisez les noms de variable
        - `largeur_max`
        - `largeur_min`
        - `largeur` qui sera modifiée
        - `volume_total`
        - `volume_etage`
    3. Pensez à votre professeur de mathématiques et pensez à écrire `* 1` dans un calcul de volume, où la hauteur vaut $1$. Le volume d'un pavé droit s'obtient en multipliant ses trois dimensions.

## 10) Le plus beau Karva

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=843&iOrder=16) est de :

> Lire plusieurs nombres, dont certains inutiles

!!! tip "Indices"
    Il faut lire **tous** les nombres, même ceux inutiles, pour que chacun soit lu dans le bon ordre.

