# ⁉️ Booléen

On a déjà croisé les booléens `#!py True` et `#!py False` en étudiant les comparaisons entre nombres.

```pycon
>>> 123 > 42
True
>>> 1.23456 <= 5.1
False
```

On peut stocker un booléen dans une variable. Ça peut simplifier légèrement du code, et c'est aussi parfois utile.

=== "Avec booléen"

    ```python hl_lines="1 5 7"
    vu = False
    for tour in range(1000):
        x = int(input())
        if x == 42:
            vu = True

    if vu:
        print("42 a été entré au moins une fois.")
    else:
        print("42 n'a jamais été entré.")
    ```

=== "Sans booléen"

    ```python hl_lines="1 5 7"
    nb_vus = 0
    for tour in range(1000):
        x = int(input())
        if x == 42:
            nb_vus = nb_vus + 1

    if nb_vus > 0:
        print("42 a été entré au moins une fois.")
    else:
        print("42 n'a jamais été entré.")
    ```

=== "Mauvaise pratique"

    ```python hl_lines="7"
    vu = False
    for tour in range(1000):
        x = int(input())
        if x == 42:
            vu = True

    if vu == True:  # Attention, (1)
        print("42 a été entré au moins une fois.")
    else:
        print("42 n'a jamais été entré.")
    ```

    1. Il est inutile, voire nuisible d'écrire `== True`

    Ce code fonctionne, mais `#!python vu == True` est un pléonasme.
    
    C'est donc redondant, on préfère sans `== True`.


## Opérateurs

Pour créer des tests élaborés on utilise des constructions à base de « ou », de « et » et de négation.

* `#!python or` est le mot clé pour un « ou logique ».
* `#!python and` est le mot clé pour un « et logique ».
* `#!python not` est le mot clé pour une « négation de ».


!!! example "Examples"
    On suppose qu'on a un rectangle avec `largeur` et `hauteur` connues.
    
    1. On veut tester si « la hauteur est supérieure à $140$, ==**et**== l'aire inférieure à $300$ », et écrire `Valide 1` dans ce cas.
      
        ```python
        aire = largeur * hauteur
        if (largeur > 140) and (aire < 300):
            print("Valide 1")
        ```
    2. On veut tester si « un côté ==**ou**== l'autre mesure 256 », et afficher `Valide 2` dans ce cas.
      
        ```python
        if (largeur == 256) or (hauteur == 256):
            print("Valide 2")
        ```
    3. On veut tester la ==**négation**== de « l'aire est inférieure à $30$, **ou** chaque côté est inférieur à $6$ », et écrire `Valide 3` si cette négation est réalisée.
      
        ```python
        aire = largeur * hauteur
        if not((aire < 30) or ((largeur < 6) and (hauteur < 6))):
            print("Valide 3")
        ```

!!! bug "Code faux"
    Pour traduire « si chaque côté est inférieur à $6$ » : `#py! if largeur and hauteur < 6:` est ==**un code Faux**==

    Il faut tester chaque côté ! `#py! if (largeur < 6) and (hauteur < 6):`


!!! warning "Humour, un petit malin"
    ![TrueFalse](../images/True-False.png)
