# 👾 France-IOI (I-7)

Le niveau 1 de France-IOI se poursuit avec [Conditions avancées, opérateurs booléens](http://www.france-ioi.org/algo/chapter.php?idChapter=648){target=_blank}

On ne donnera de l'aide que pour la lecture de l'entrée et l'affichage de la sortie.

## 1) Espion étranger

```python
date_debut = int(input())
date_fin = int(input())
nb_entree = int(input())
...
RÉPÉTER ??? FOIS
    date_entre = int(input())
    ...

print(nb_suspects)
```


## 2) Maison de l'espion

```python
x_min = int(input())
x_max = int(input())
y_min = int(input())
y_max = int(input())

nb_maisons = int(input())
RÉPÉTER ??? FOIS
    x = int(input())
    y = int(input())
    ...

print(nb_suspects)
```

## 3) Nombre de jours dans le mois

!!! tip "Conseil"
    N'utiliser qu'un seul `print` ; à la fin

    Pourquoi ? Si un jour vous souhaitez faire autre chose avec, il suffit de modifier le code à un seul endroit. On parle de factorisation du code.

```python
num_mois = int(input())

...

print(nb_jours)
```


## 4) Amitié entre gardes

!!! tip "Indice"
    Il est **beaucoup** plus simple de trouver les cas qui donnent `Pas amis`.

    Les autres cas donneront `Amis`

```python
debut_1 = int(input())
fin_1 = int(input())
debut_2 = int(input())
fin_2 = int(input())


print("Pas amis")
print("Amis")
```


## 5) Nombre de personnes à la fête

```python
nb_personnes = int(input())
nb_presents = ...
nb_max_presents = ...
RÉPÉTER ??? FOIS
    entier = int(input())   # positif ou négatif
    ...

print(nb_max_presents)
```



## 6) Casernes de pompiers

!!! tip "Indice"
    Revoir le problème "Amitié entre gardes"

```python
nb_paires_zones = int(input())

RÉPÉTER ??? FOIS
    x_min_1 = int(input())
    x_max_1 = int(input())
    y_min_1 = int(input())
    y_max_1 = int(input())

    x_min_2 = int(input())
    x_max_2 = int(input())
    y_min_2 = int(input())
    y_max_2 = int(input())

    ...
    print("NON")
    ...
    print("OUI")
```

## 7) Personne disparue

!!! tip "Indice"
    On peut stocker un booléen dans une variable, par exemple

    ```python
    fini = False
    est_premier = True
    ```

Modifier le code

```python
numero_recherche = int(input())
taille_liste = int(input())

RÉPÉTER ??? FOIS
   numero = int(input())

print("Sorti de la ville")
print("Encore dans la ville")
```

## 8) La grande fête

```python
debut_periode = int(input())
fin_periode = int(input())
nb_invites = int(input())

RÉPÉTER ??? FOIS
   debut = int(input())
   fin = int(input())


print(nb_suspects)
```


## 9) L'espion est démasqué !


```python
nb_personnes = int(input())
RÉPÉTER ??? FOIS

    taille = int(input())
    age = int(input())
    poids = int(input())
    a_cheval = int(input())
    est_brun = int(input())

    ......nb_criteres....
    print("Impossible")
    print("Très probable")
    print("Probable")
    print("Peu probable")
```


## 10) Zones de couleurs

!!! tip "Indice"
    Pour simplifier le code, 
    
    - on pourra commencer par afficher jaune dans le cas de la **zone intérieure**,
    - on n'oubliera pas la zone hors de la feuille, comme les autres cas,
    - et on finira par le reste du jaune.

```python
nb_jetons = int(input())
RÉPÉTER ??? FOIS
    x = int(input())
    y = int(input())


    print("En dehors de la feuille")
    print("Dans une zone rouge")
    print("Dans une zone bleue")
    print("Dans une zone jaune")
```

