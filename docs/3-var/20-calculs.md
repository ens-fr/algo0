# ➕ Calculs simples

1. Les variables peuvent stocker des résultats issus de calculs.
2. On peut afficher le contenu d'une variable.

Commençons par des calculs simples.

!!! tip "Avec Python, pour deux entiers"

    | Opération      |Opérateur|
    |:---------------|:-------:|
    | Addition       | `+`  |
    | Soustraction   | `-`  |
    | Multiplication | `*`  |


# Modifier une variable

Anticiper, étudier et expliquer les différentes sorties

=== "Cas n°1"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    largeur = 7
    aire = largeur * hauteur
    print(aire)
    ```

    ??? success "Résultat"
        
        ```output
        20
        28
        ```

    ??? info "Explication"
        1. L'`aire` est calculée avec $5 × 4$, puis affichée. $20$
        2. L'`aire` est à nouveau calculée avec $7 × 4$, puis affichée. $28$.

        La `hauteur` n'a pas été modifiée, mais la `largeur` oui.

        La variable `aire` a aussi été modifiée.


=== "Cas n°2"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    hauteur = hauteur + 3
    print(aire)
    ```

    ??? success "Résultat"
        
        ```output
        20
        20
        ```

    ??? info "Explication"
        1. L'`aire` est calculée **une seule fois**, mais affichée deux fois avec la même valeur.
        2. Oui, la `hauteur` a été modifiée de $4$ à $7$.

        ```python
        hauteur = hauteur + 3
        ```

        Ce code se lit : « la nouvelle valeur de `hauteur` est égale à l'ancienne valeur de `hauteur` plus $3$ ».



=== "Cas n°3"

    ```python
    hauteur = 4
    largeur = 5
    aire = largeur * hauteur
    print(aire)
    hauteur = hauteur + 3
    largeur = largeur - 2
    aire = largeur * hauteur
    print(aire)
    ```

    ??? success "Résultat"
        
        ```output
        20
        21
        ```

    ??? example "Explication"
        L'`aire` est calculée puis affichée deux fois.

        1. Première fois avec $4 × 5$ qui donne $20$.
        2. Deuxième fois avec $7 × 3$ qui donne $21$.

        En effet,

        1. `hauteur` a augmenté de $3$. Passe de $4$ à $7$.
        2. `largeur` a diminué de $2$. Passe de $5$ à $3$.


## Répéter la modification d'une variable

### Exemple 1

Étudions l'exemple ci-dessous

```python
hauteur = 1
for tour in range(4):
    hauteur = hauteur + 3
print(hauteur)
```

```output
13
```

!!! info "Justification"
    `hauteur` a augmenté $4$ fois de $3$, donc de $12$.
    
    En partant de $1$, on arrive à $13$.



### Exemple 2

```python
somme = 0
n = 0
for tour in range(4):
    n = n + 1
    augmentation = n * n
    somme = somme + augmentation
print(somme)
```

```output
30
```

!!! info "Justification"
    1. `somme` a été initialisée à zéro, puis a subi 4 augmentations.
    2. Les augmentations ont été de $1×1$, puis $2×2$, puis $3×3$, et enfin $4×4$.

    $$1×1 + 2×2 + 3×3 + 4×4 = 30$$

    Cette technique est très utilisée pour le cumul de plusieurs valeurs.
