# ☑️ Bon identifiant - Exercice

## Identifiant valide

Pour ce premier exercice, on ne regarde que si l'identifiant est valide,
 il pourrait être mal choisi.

=== "Cocher les identifiants valides"

    - [ ] `as`
    - [ ] `Roi`
    - [ ] `2ame`
    - [ ] `v413t`
    - [ ] `dix`
    - [ ] `n'œuf`
    - [ ] `huit`
    - [ ] `Sète`
    - [ ] `carte_six`
    - [ ] `_5`
    - [ ] `%4`
    - [ ] `quatre-moins-un`
    - [ ] `2!`
    - [ ] `_`

=== "Réponse"

    - ❌ `as` ; c'est un mot-clé, il est réservé.
    - ✅ `Roi`
    - ❌ `2ame` ; interdit de commencer par un chiffre.
    - ✅ `v413t`
    - ✅ `dix`
    - ❌ `n'œuf` ; interdit d'utiliser `'`
    - ✅ `huit`
    - ✅ `Sète`
    - ✅ `carte_six`
    - ✅ `_5`
    - ❌ `%4` ; interdit d'utiliser `%`
    - ❌ `quatre-moins-un` ; interdit d'utiliser `-`
    - ❌ `2!` ; interdit d'utiliser `!`
    - ✅ `_`

## Meilleur identifiant

Pour les questions suivantes, on souhaite avoir le meilleur identifiant valide.

- Cocher le meilleur identifiant pour une variable.

### Question 1

=== "Propositions"

    - [ ] `pv`
    - [ ] `p_vie`
    - [ ] `points_vie`
    - [ ] `les_points_de_vie`

=== "Réponse"

    - ❌ `pv` ; trop ambigu
    - ❌ `p_vie` ; assez ambigu
    - ✅ `points_vie` ; ou alors `pt_vie`
    - ❌ `les_points_de_vie` ; trop long

### Question 2

=== "Propositions"

    - [ ] `nombre_e`
    - [ ] `nb_essais`
    - [ ] `nombre_essai`
    - [ ] `e`

=== "Réponse"

    - ❌ `nombre_e` ; ce n'est pas le bon mot à abréger
    - ✅ `nb_essais`
    - ❌ `nombre_essai` ; trop long, et il faudrait un `s` à la fin
    - ❌ `e` ; trop ambigu

### Question 3

=== "Propositions"

    - [ ] `AireFigure`
    - [ ] `airefigure`
    - [ ] `a_f`
    - [ ] `aire_figure`

=== "Réponse"

    - ❌ `AireFigure`
    - ❌ `airefigure`
    - ❌ `a_f`
    - ✅ `aire_figure`

### Question 4

=== "Propositions"

    - [ ] `distance_AB`
    - [ ] `distance_du_point_A_au_point_B`
    - [ ] `dAB`
    - [ ] `distance`

=== "Réponse"

    - ✅ `distance_AB`
    - ❌ `distance_du_point_A_au_point_B`
    - ❌ `dAB`
    - ❌ `distance`

### Question 5

=== "Propositions"

    - [ ] `est_un_nombre_premier`
    - [ ] `premier`
    - [ ] `estUnNombrePremier`
    - [ ] `est_premier`

=== "Réponse"

    - ❌ `est_un_nombre_premier`
    - ❌ `premier`
    - ❌ `estUnNombrePremier`
    - ✅ `est_premier`
