# 👾 France-IOI (I-3)

Le niveau 1 de France-IOI se poursuit avec [Calculs et découverte des variables](http://www.france-ioi.org/algo/chapter.php?idChapter=644){target=_blank}

## 1) Réponds !

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1863) est de :

> Afficher le nombre 42. 

!!! info "Culture Geek"
    Il s'agit d'une référence à un feuilleton radio de science-fiction humoristique, _Le Guide du voyageur galactique_, écrit par Douglas Adams.


## 2) L'éclipse

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1864) est de :

> Afficher le résultat d'un calcul simple.


!!! tip "Indices"
    1. Utiliser des variables, même si on peut faire sans !
    2. Votre solution doit pouvoir se lire et se comprendre sans lire l'énoncé.
    3. Ne pas mettre de guillemets pour l'affichage d'une variable.

## 3) Bonbons pour tout le monde !

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=2007) est de :

> Calculer le nombres de bonbons.

!!! tip "Indices"
    1. Ne pas faire soi-même les opérations ; c'est pour Python !
    1. Utiliser des variables, même si on peut faire sans !
    2. Votre solution doit pouvoir se lire et se comprendre sans lire l'énoncé.
    3. Ne pas mettre de guillemets pour l'affichage d'une variable.


## 4) L'algoréathlon

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=7) est de :

> Afficher trois valeurs à la suite sur une ligne.

!!! tip "Indices"
    Rappel : pour afficher sur une ligne le contenu de `var_1`, `var_2` et `var_3`, en les séparant d'une espace, on peut faire :

    === "Méthode 1, recommandée"

        ```python
        print(var_1, var_2, var_3)
        ```

        Python affiche les valeurs en les séparant (par défaut) d'une espace, et finit par un saut de ligne (par défaut).

    === "Méthode 2, possible"

        ```python
        print(var_1, end=" ")
        print(var_2, end=" ")
        print(var_3)
        ```
    
        Python affiche les deux premières valeurs, sans faire le saut de ligne, qui est remplacé par une espace. Enfin il affiche la dernière avec un saut de ligne.

## 5) Cour de récréation

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&idTask=1886) est de :

> Afficher deux lignes : l'aire d'un carré, puis son périmètre.

!!! tip "Indices"
    1. Utiliser des variables : `cote`, `aire`, `perimetre`.
    2. Mettre un commentaire pour l'unité
    3. Votre solution doit pouvoir se lire et se comprendre sans lire l'énoncé.


## 6) Une partie de cache-cache

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=11) est de :

> Compter jusqu'à cent.

!!! tip "Indice"
    1. Combien de tour de boucles doit-on faire ?
    2. Quelle valeur sera affichée en premier ?
    3. Votre variable sera-t-elle bien modifiée au tour suivant ?


## 7) Progresser par l'erreur

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=13) est de :

> Trouver les scripts **V**alides et ceux qui sont **I**nvalides.

!!! tip "Indice"
    Il faut afficher
    
    - `V` si le script est valide, même s'il n'est pas très beau.
    - `I` s'il est invalide.

    Modifier le code en enlevant un caractère (`V` ou `I`) par ligne

    ```python
    print("VI")
    print("VI")
    print("VI")
    print("VI")
    print("VI")
    print("VI")
    print("VI")
    ```
    
## 8) Décollage de fusée

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=14) est de :

> Afficher un compte à rebours.

!!! tip "Indices"
    1. Combien de tour de boucles doit-on faire ? Sûr ?
    2. Quelle valeur sera affichée en premier ?
    3. Votre variable sera-t-elle bien modifiée au tour suivant ?

## 9) Invasion de batraciens

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=15) est de :

> Calculer avec une boucle une progression géométrique.

!!! tip "Indice"
    1. Utiliser une variable bien nommée.
    2. N'afficher qu'un seul nombre. À la fin.


## 10) Kermesse

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=16) est de :

> Faire la somme cumulée de termes.

!!! tip "Indices 1"
    1. Préparer une boucle pour faire une somme cumulée.
    2. Utiliser une variable `augmentation`
    3. Tester vos variables au début des premières boucles.

??? tip "Indices 2"
    Modifier le code

    ```python
    somme = ???
    augmentation = ???
    RÉPÉTER ??? FOIS
        print(somme)
        somme = somme + augmentation
        augmentation = ???
    ```

## 11) Course avec les enfants

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=18) est de :

> Aller chercher et ramener des anneaux à des distances variables.

!!! tip "Indices 1"
    1. Ne pas oublier `#!python from robot import *`
    2. Écrire votre algorithme en français et remplacer progressivement par du Python.

??? tip "Indices 2"
    Modifier le code

    ```python
    from robot import *

    distance = ???
    RÉPÉTER ??? FOIS
        aller à droite `distance` fois
        prendre anneau
        aller à gauche `distance` fois
        poser anneau
        modifier `distance`
    ```

??? tip "Indices 3"
    Pour aller à droite (ou à gauche) plusieurs fois, on utilise une boucle.

    Il est possible d'utiliser `#!python range(distance)`


## 12) Construction d'une pyramide

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=19) est de :

> Compter le nombre de cubes dans une pyramide

!!! tip "Indices 1"
    1. Compter le nombre exact de tours de boucle.
    2. Préparer une boucle pour faire une somme cumulée.
    3. Utiliser les variables `volume_total`, `volume_cube` et `taille`

??? tip "Indices 2"
    Modifier le code

    ```python
    volume_total = ???
    taille = ???
    RÉPÉTER ??? FOIS
        volume_cube = ???
        volume_total = ???

    print(volume_total)
    ```

## 13) Table de multiplication

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=644&iOrder=20) est de :

> Afficher la table de multiplication allant jusqu'à $20×20$.

!!! tip "Indices"
    1. Préparer une boucle pour afficher une ligne
    2. Répéter cette boucle pour afficher toutes les lignes.

