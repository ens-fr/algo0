# ⚗️ Variable

## À savoir

!!! note "Le principe"
    - On peut désigner des résultats par des variables pour les réutiliser plus tard.
    - En maths, on n'utilise souvent qu'une lettre (quitte à la prendre dans un autre alphabet), en programmation on préfère utiliser des variables avec un nom le plus explicite possible, qu'on peut taper facilement au clavier.

!!! question "Comment choisir son nom de variable ?"
    - **Règle 1** : N'utiliser que les caractères : `a→z`, `_`, `A→Z` et `0→9`
    - **Règle 2** : Ne pas débuter par un chiffre.
    - **Recommandation** :
        - `choisir_un_nom_de_variable_lisible`
        - `NePaschoisirUnNomDeVariablePeuLisible`
        - `nepaschoisirunnomdevariableillisible`
        - `JE_SUIS_UNE_JOLIE_CONSTANTE`
    - **Exemples** :
        - `nb_triangles`
        - `x_0`
        - ces noms de variable sont valides et lisibles.
    - **Conseil** : on limitera les abréviations à
        - `nb` pour « nombre de ...»,
        - `i_` pour « indice de ...»,
    - **Cas de Python** :
        - Les lettres accentuées sont autorisées avec Python, mais pas avec les autres langages de programmation. Pour avoir de bonnes habitudes généralistes, il vaut mieux ne pas les utiliser.

!!! info "Tiret bas et *snake_case*"
    `_` est le tiret bas

    - Il ne faut pas dire « tiret du 8 », car cela dépend du clavier...
    - Il a de nombreuses utilisations [^1] en informatique. Il est idéal pour séparer des mots dans un nom de variable élaboré. Très utile. On parle de *snake_case* [^2]

[^1]: :fontawesome-brands-wikipedia-w: Le [titer bas](https://fr.wikipedia.org/wiki/Tiret_bas){ target=_blank } a de nombreuses utilisations en informatique.

[^2]: :fontawesome-brands-wikipedia-w: Le [*snake_case*](https://fr.wikipedia.org/wiki/Snake_case){ target=_blank } est utilisé avec Python pour les noms de variable. On utilise le CamelCase pour d'autres usages.

!!! tip "Mots-clés de Python3"
    Il y a **35 mots-clés** qui sont réservés (dont deux récents) qui
     ne peuvent donc pas être utilisés pour des variables.

    |           |           |           |           |         |
    |:----------|:----------|:----------|:----------|:--------|
    |`#!python False`    |`#!python class`    |`#!python finally`  |`#!python is`       |`#!python return` |
    |`#!python None`     |`#!python continue` |`#!python for`      |`#!python lambda`   |`#!python try`    |
    |`#!python True`     |`#!python def`      |`#!python from`     |`#!python nonlocal` |`#!python while`  |
    |`#!python and`      |`#!python del`      |`#!python global`   |`#!python not`      |`#!python with`   |
    |`#!python as`       |`#!python elif`     |`#!python if`       |`#!python or`       |`#!python yield`  |
    |`#!python assert`   |`#!python else`     |`#!python import`   |`#!python pass`     |`#!python async`  |
    |`#!python break`    |`#!python except`   |`#!python in`       |`#!python raise`    |`#!python await`  |

    !!! abstract "Remarques"
        Seuls `#!python False`, `#!python None` et `#!python True` commencent ici avec une majuscule.

        Faut-il connaitre ces 35 noms par chœur ? **Non**, seulement 3.

        1. Si on essaie de s'en servir comme un identifiant une erreur est levée.
        2. En codant en **français usuel**, peu de mots sont en commun avec cette liste.
        3. Certains mots-clé sont **très** utilisés, comme `#!python if`, `#!python or` et `#!python import`
        4. Il n'y a donc que 3 mots-clé à retenir et ne pas utiliser en identifiant :
            - `#!python as`
            - `#!python continue`
            - `#!python global`

## Exemples

```pycon
>>> prix_unitaire = 125      # en €
>>> quantité = 12
>>> prix_total = quantité * prix_unitaire
>>> prix_total
1500
```

!!! question "Pourquoi un seul résultat affiché ?"
    Dans la console Python, une affectation ne produit pas d'affichage.

    La dernière instruction est un *calcul simple* évalué, et
    
    - **la console affiche le résultat d'une évaluation, pas d'une affectation**.

```pycon
>>> platine = 32_005         # € / kg
```

!!! question "Les commentaires avec `#!python #`"
    On place un commentaire au moins **deux espaces après le code**,

    - ensuite un croisillon[^3] `#`
        et un**e** espace[^4],
    - enfin son **court commentaire**.

    Il sera ignoré par Python, mais il pourra être utile au lecteur.

    Il est préférable que le code soit lisible sans.

    Pour les unités, c'est une bonne pratique.

    On évite de faire de longues lignes ; 79 caractères au maximum, c'est mieux.

    On peut placer un commentaire seul sur une ligne avant le code qu'il détaille.

[^3]: :fontawesome-brands-wikipedia-w: Le [croisillon](https://fr.wikipedia.org/wiki/Croisillon_(signe)){ target=_blank } `#` n'est pas un dièse `♯`.
[^4]: :fontawesome-brands-wikipedia-w: [L'espace](https://fr.wikipedia.org/wiki/Espace_(typographie)){ target=_blank } est féminin en typographie.

```pycon
>>> or = 47_300  # € / kg
  File "<stdin>", line 1
    or = 47_300  # € / kg
    ^
SyntaxError: invalid syntax
```

!!! question "SyntaxError"
    `#!python or` est un mot-clé, on ne peut pas l'utiliser comme identifiant.

    `#!python SyntaxError` signifie « erreur de syntaxe », et la flèche indique l'origine.

    :warning: attention, parfois l'origine de l'erreur est sur la ligne précédente,
    par exemple, quand il manque une parenthèse.

## En savoir plus

!!! example "Nous allons bientôt découvrir"
    - `def` pour définir une fonction.
    - `return` pour une valeur que la fonction **renvoie**.
    - `if`, `elif`, `else` : pour les structures conditionnelles.
    - `for`, `in`, `while` : pour créer des boucles.
    - `and`, `or`, `not` : opérateurs sur les booléens : **et**, **ou**, **non**.
    - `import` et `from` : pour **importer à partir** d'un module.
    - `None` : pour une donnée _sans valeur_ ; zéro étant un entier possédant une valeur.
    - `assert` : pour le débogage facile ; on vérifie un état.
    - `raise` : pour provoquer volontairement une erreur contrôlée.

!!! warning "Utilisation avancée"
    On ne recommande pas leur utilisation aux grands débutants.

    - `pass` : une instruction qui ne fait rien.
    - `in` : pour un test d'appartenance.
    - `is` : aussi pour test d'identité entre objets.
    - `del` : pour détruire (*<strong>del</strong>ete*) une variable.
    - `lambda` : pour définir une fonction anonyme.
    - `break`, `continue` : pour les boucles plus complexes.
    - `try`, `except` : pour la gestion des erreurs.
    - `with`, `as` : pour la lecture de module ou de fichier.
    - `class` : pour la programmation orientée objet (POO).
    - `global`, `nonlocal` : pour modifier la portée d'une variable.
    - `yield` : pour la construction d'itérateur.
