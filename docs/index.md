# 🏡 Accueil

La découverte de Python.

On reprend la progression du niveau 1 de France-IOI. Elle est adaptée à tous les débutants en programmation.

![logo Python](./images/logo-python.svg){width=300}

{{ terminal() }}

> Un grand remerciement à M. Vincent BOUILLOT pour le portage de [Pyodide](https://pyodide.org/en/stable/) vers MkDocs, cela permet d'utiliser Python encore plus simplement qu'avec [Basthon](https://basthon.fr/){ target=_blank }.


!!! example "Des calculs simples"
    
    ![Exemples avec divisions](./images/divisions.svg)


## Sommaire

On propose un cours moderne, qui peut être lu facilement sur tablette, téléphone ou ordinateur.

Ce document reprend la progression sur France-IOI, niveau 1.

!!! tip "France-IOI"
    Si ce n'est pas déjà fait, s'inscrire sur le site [France-IOI](http://www.france-ioi.org/index.php), cela vous permet de conserver la trace de votre progression. Il est vivement conseillé de s'y exercer régulièrement.

    > [France-IOI est une association](http://www.france-ioi.org/asso/index.php) à but non lucratif (loi 1901) fondée en juin 2004 dans le but de développer la sélection et l'entrainement de l'équipe de France aux Olympiades Internationales d'Informatique.

    Le site est aussi adapté à la découverte de la programmation, avec le langage Python. Le niveau 1 est accessible à tous les élèves du lycée. Les niveaux 2 et 3 intéressent les élèves en spé maths ou NSI. Les niveaux 3, 4 et 5 proposent un joli contenu pour les élèves de terminale NSI.

!!! success "Bientôt un jeu"
    Quand vous aurez presque **fini** cette section `Découverte`, il vous sera possible de faire jouer un personnage à un jeu grâce à Python.

    ![pyRATES](images/pyrates.png)

!!! tip "D'autres ressources en parallèles"
    On conseille fortement de s'entrainer aussi avec Blockly Games. Ce n'est pas du Python, mais la démarche algorithmique est la même. Les exercices sont progressifs. Il n'y a presque pas besoin de clavier. C'est une initiation très bien menée.

    Voici une version locale hébergée par nos propres soins, elle respecte **totalement** le RGPD.

    [Blocky Games en français](./blockly-games/index.html){ .md-button }


---

!!! info "À propos de ce site - RGPD"
    - Aucun _cookie_ n'est créé.
    - Il n'y a aucun lien vers des pisteurs.
        - Pas de polices Google qui espionne.
        - Pas de CDN malicieux.
    - Le langage Python est émulé par Pyodide sur votre propre machine.
        - Vous n'avez besoin de rien installer.
        - Strictement aucun code, aucune donnée personnelle n'est envoyée.
    - Adapté pour les PC, les tablettes et aussi les téléphones. 
    - Très peu gourmand en ressources, les pages sont légères.
    - Code source du contenu sous licence libre, sans utilisation commerciale possible.
