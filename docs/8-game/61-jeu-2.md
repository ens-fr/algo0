# ⚔️ Jeu 2 : avec deux joueurs

!!! tip "Astuce"
    Pour éviter la triche, on peut demander à Python de faire des vérifications.

    Si on demande à un joueur un nombre de 1 à 3, on peut demander à Python de vérifier cela avec `#py assert`. Si c'est bon, le programme continue normalement, sinon le programme s'arrête.

!!! example "Exemple"

    ```python
    nombre = int(input())
    assert 1 <= nombre <= 3, "On avait dit de 1 à 3 !"
    # suite...
    ```

    Si le `nombre` est bien de 1 à 3, le programme se poursuit.

    Sinon, le programme s'arrête avec `#py AssertionError` et le message `On avait dit de 1 à 3 !`


Vous devez créer un jeu avec un programme Python :

1.  Le programme affiche les règles du jeu.
2.  Le programme demande un nombre entre 10 et 30 : le nombre de barres au départ.
3.  À tour de rôle, les joueurs peuvent enlever 1, 2 ou 3 barres, mais toujours en laisser au moins une. Le programme demande combien chaque joueur souhaite en enlever.
4.  Celui qui ne peut plus jouer a perdu. Le programme affiche le vainqueur.

Exemple de partie :

```output
Bienvenue au donjon.

Voici un jeu à deux joueurs, où :
- il faut enlever à tour de rôle 1, 2 ou 3 barres,
- il faut toujours en laisser au moins une.
Celui qui ne peut plus jouer a perdu.

Combien de barres au départ ? 12

===== C'est parti =====

IIIIIIIIIIII

Combien joueur 1 en enlève ? 3

IIIIIIIII

Combien joueur 2 en enlève ? 2

IIIIIII

Combien joueur 1 en enlève ? 3

IIII

Combien joueur 2 en enlève ? 3

II

Combien joueur 1 en enlève ? 1

I

Le joueur 2 ne peut plus jouer.
Le joueur 1 a gagné. Bravo !
```


Vous pouvez utiliser les fragments de code.

{{ IDE('barres') }}


!!! success "Jeu fini"
    Quand votre jeu est terminé,

    - vous pouvez le télécharger
    - vous pouvez l'envoyer à un ami ou votre professeur
    - vous pourrez le recharger dans tout interpréteur Python

    Bravo.
