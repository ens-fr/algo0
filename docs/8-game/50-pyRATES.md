# 🏴‍☠️ pyRATES

!!! success "BRAVO"
    Avec Python, vous savez désormais :

    - Utiliser des variables et enchainer des calculs simples
    - Lire et écrire des données numériques
    - Répéter une action un certain nombre de fois
    - Utiliser une condition pour choisir ses actions
    - Répéter une action tant qu'une condition est réalisée


Vous pouvez désormais tenter les chalenges de pyRATES, en cliquant sur l'image.

[![pyRATES](../images/pyrates.png)](https://py-rates.fr/){ target=_blank }

Il vous faudra raisonnablement entre 20 minutes et un peu plus d'une heure.

Un remerciement à l'auteur de ce jeu

> Matthieu BRANTHÔME  
> Doctorant en informatique / sciences de l'éducation  
> Université de Bretagne Occidentale

## La suite

!!! info "Créateur de jeux vidéos"
    Jouer, c'est bien, mais créer c'est encore mieux !
    
    Mais soyons honnêtes, il ne sera pas possible très vite de créer des jeux en 3D avec beaucoup d'actions...

    En revanche, il sera possible de créer des petits jeux simples, en mode texte, à un ou deux joueurs. Plus le jeu sera complexe, et plus il faudra utiliser de mathématiques pour le construire efficacement.


Il faudra poursuivre avec plus de cours de programmation.

Mais, dès à présent, il est possible de créer des jeux très simples.
