# ♟️ Autres jeux

Il faudra apprendre à créer et utiliser les tableaux.

- Pour un jeu de cartes, le tas de carte sera un tableau.
- Une image sera un tableau de lignes et chaque ligne sera un tableau de pixels.
- De même pour une grille ou pour tout jeu de plateau.

Il faudra apprendre à créer et utiliser les fonctions.

- On pourra découper le programme en morceaux réutilisables.
- On pourra mieux comprendre la structure des enchainements.

Pour des jeux avec de la 3D, il faudra encore plus d'expériences.

Mais la programmation et l'algorithmique ne servent pas qu'à créer des jeux.
