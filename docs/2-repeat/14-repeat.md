# 🔁 Répétition d'instructions

Le premier gros avantage de l'informatique, c'est de pouvoir **automatiser** des tâches répétitives.

## Répéter n fois

Avec Python, pour répéter $n$ fois une même `action()`, on peut écrire

```python
for i in range(n):
    action()
```

## Première utilité

Pour déplacer un robot de 10 cases à droite, deux méthodes.

=== "Avec boucle"

    ```python
    from robot import *

    for i in range(10):
        droite()
    ```

=== "Sans boucle"

    ```python
    from robot import *

    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    action()
    ```

Faire une boucle est plus rapide à écrire, plus facile à maintenir... Imaginez avec 837 répétitions...


```python
for i in range(nb_tours):
    ######################
    #                    #
    des_actions()        #
    #                    #
    ######################
suite_du_programme()
```



!!! abstract "La syntaxe"
    * On commence par `#!python for`, un mot clé du langage Python.
    * Ensuite, on choisit un **nom de variable**.
        - C'est bien de choisir un nom qui a du sens, mais pas un mot-clé.
        - On recommande `i` pour commencer, comme **i**ndice.
    * Ensuite, il y a `#!python in` ; un mot-clé du langage.
    * Puis `#!python range(...)` dans lequel on écrit le nombre de tours à faire.
    * **Très important**, on termine la ligne par `:`

    Les lignes suivantes, du bloc d'instructions que l'on veut répéter doivent être **indentées**. On parle d'indentation du code. Ce décalage s'obtient avec la touche ++tab++

    On revient au **même niveau** que le `#!python for` initial, pour la **suite** des instructions.

## Exemples

Essayez chacun des exemples, vous pouvez les modifier.

=== "Exemple 1"

    {{ IDEv('for1') }}

=== "Exemple 2"

    {{ IDEv('for2') }}

=== "Exemple 3"

    {{ IDEv('for3') }}

## Exercice 1

{{ IDEv() }}

Votre objectif est d'afficher le texte ci-dessous en faisant des boucles.

La largeur est de 51 (mais imaginez qu'on vous demande 5001 bientôt).

=== "Situation 1"

    ```output
    ***************************************************
    ```

    Une boucle suffit.

    ??? "Réponse"

        ```python
        for i in range(51):
            print("*", end="")
        print()
        ```

=== "Situation 2"

    ```output
    ***************************************************
    *                        O                        *
    ***************************************************
    ```

    ??? "Réponse"

        ```python
        # première ligne
        for i in range(51):
            print("*", end="")
        print()

        # Deuxième ligne
        print("*", end="")
        for i in range(24):
            print(" ", end="")
        print("*", end="")
        for i in range(24):
            print(" ", end="")
        print("*")

        # Troisième ligne
        for i in range(51):
            print("*", end="")
        print()
        ```

        !!! tip "Les commentaires de code"
            On peut en placer après le caractère `#` : un **croisillon**

            Ils sont ignorés par Python

            Ils aident à mieux comprendre le code que l'on va relire **ou faire lire**.



## Exercice 2

Combien de fois l'instruction `action` est-elle effectuée dans chaque cas ?

=== "Cas n°1"

    ```python
    for tour in range(10):
        action()
    ```

    ??? success "Réponse"
        $10$ ; facile.
    
=== "Cas n°2"

    ```python
    init()
    action()
    for tour in range(12):
        action()
        inter()
        action()
    fin()
    action()
    ```

    ??? success "Réponse"
        - $1$ fois avant
        - $12$ fois $2$ pendant
        - $1$ fois après

        Il y a eu $1+12×2+1=26$ fois `action()` effectuée.

=== "Cas n°3"

    ```python
    for tour_1 in range(10):
        action()
        for tour_2 in range(5):
            action()
            post()
    action()
    ```

    Oui, on peut faire une boucle dans une boucle. À méditer !

    ??? success "Réponse"
        Chaque tour de la boucle 1 est identique, il y a :
            - une `action()`
            - puis $5$ fois une `action()`
        
        Le script fait donc un nombre d'appels à `action()` égal à :
        
            - $10$ fois ($1$ plus $5$)
            - plus une dernière

        Un total de $10×(1+5) + 1 = 61$ fois `action()` effectuée.

=== "Cas n°4"

    ```python
    for tour_1 in range(1000):
        action()

    for tour_2 in range(1000):
        action()

    for tour_3 in range(1000):
        action()
    ```

    ??? success "Réponse"
        $1000+1000+1000$ donne $3000$ fois `action()` effectuée.
        
=== "Cas n°5"

    ```python
    for tour_1 in range(1000):
        for tour_2 in range(1000):
            for tour_3 in range(1000):
                action()
    ```

    ??? success "Réponse"
        Ici, c'est $1000×1000×1000$ donc un milliard de fois où l'action est effectuée. Ce qui montre le danger d'écrire ce genre de construction. À méditer !

=== "Cas n°6"

    ```python
    for tour_1 in range(1000):
        action()
        for tour_2 in range(1000):
            action()
            for tour_3 in range(1000):
                action()
    ```

    ??? success "Réponse"
        Ici, c'est $1000×(1 + 1000×(1 + 1000))$ donc $1\,001\,001\,000$
