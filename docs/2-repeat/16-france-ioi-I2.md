# 👾 France-IOI (I-2)

Le niveau 1 de France-IOI se poursuit avec [Répétitions d'instructions](http://www.france-ioi.org/algo/chapter.php?idChapter=643){target=_blank}

## 1) Punition

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1876) est de :

> Écrire $135$ fois la phrase : `Je dois respecter le Grand Sorcier.`

!!! tip "Indice 1"
    Compléter le code

    ```python
    ...
    ...   Je dois respecter le Grand Sorcier.   ...
    ```

??? tip "Indice 2"
    Pour afficher 5 fois `Coucou`

    ```python
    for _ in range(5):         # conseil 1 (1)
        print("Coucou")        # conseil 2 (2)
    ```

    1. Il ne faut pas oublier `:` à la fin
    2. Il faut indenter cette ligne. 4 espaces idéalement.

## 2) Mathématiques de base

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=4) est de :

> Corriger les erreurs contenues dans le programme ci-dessous afin qu'il affiche $13$ fois de suite le texte `9 * 8 = 72`

```python
for loop in range(13)
print("9 * 8 = 72)
```

??? tip "Indices"
    Il y a **trois** erreurs, mais

    - On peut remplacer `loop` par `_`, ou par `tour`, ou par `i` ; ce n'est pas une erreur.
    - $9×8$ est bien égal à $72$ ; ce n'est pas une erreur.

## 3) Transport d'eau

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=5) est de :

Bien suivre les indications demandées.

Compléter le code

```python
from robot import *

gauche()
...
print("...")
for i in range...
    ...
...
```

!!! tip "Indices"
    - Bien relire ce qui est demandé.
    - **Ne rien faire de plus.**

## 4) Le secret du Goma

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&idTask=1879) est de :

> Ramasser et déposer des bouses.

!!! tip "Indices"
    Modifier et compléter le code

    ```python
    from robot import *

    RÉPÉTER ??? FOIS
        ...
        ...
    ...
    ...
    ```

## 5) Sisyphe

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=10) est de :

> Monter et descendre $21$ marches.

??? tip "Indice"
    Modifier et compléter le code

    ```python
    from robot import *

    # monter
    RÉPÉTER ??? FOIS
        ...
        ...
    
    # redescendre
    RÉPÉTER ??? FOIS
        ...
        ...
    ```

## 6) Page d'écriture

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=13) est de :

> Écrire $3$ lignes avec $30$ lettres chacune sur le modèle

```output
a_a_a_a_
b_b_b_b_
c_c_c_c_
```

!!! tip "Indice 1"
    Pour afficher `a_` sans retour à la ligne on peut faire, au choix
    
    - `#!py print("a_", end="")`
    - `#!py print("a", end="_")`
    - `#!py print("", end="a_")`
    
    Pour sauter à la ligne, on peut faire, au choix

    - `#!py print("")`
    - `#!py print()`

??? tip "Indice 2"
    Modifier et compléter le code

    ```python
    RÉPÉTER ??? FOIS
        print("a", end="_")
    SAUT DE LIGNE

    ...
        ...b...

    ...
        ...c...
    ```

    :warning: Ne **pas** écrire `from robot import *` pour ce problème !


## 7) Jeu de dames

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=15) est de :

> Afficher un damier de taille $40×40$.

!!! tip "Indices"
    - Commencer par afficher **une** ligne qui commence par `OX`
    - Continuer par afficher **une** ligne qui commence par `XO`
    
    Combien de caractères affichez-vous par ligne ? Êtes-vous sûr ?

    Répéter ces deux étapes pour avoir les $40$ lignes. Combien de répétitions ? Êtes-vous sûr ?

## 8) Mont Kailash

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=16) est de :

> Faire faire au robot $108$ fois le tour de la montagne.

!!! tip "Indice"
    Compléter le code pour faire **un** tour

    ```python
    from robot import *

    ...faire un tour...
    ```

    Ensuite, quand vous savez **bien** faire un tour :
    
    - Sélectionner **tout** le code, sauf la première ligne.
    - Appuyer sur la touche tabulation ++tab++
    - Il est alors indenté !
    - Modifier alors le code pour faire plusieurs tours de la montagne

    ```python
    from robot import *

    RÉPÉTER ??? FOIS
        ...faire un tour...
    ```


## 9) Vendanges

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=18) est de :

> Ramasser du raisin pour le déposer à droite.

!!! tip "Indice 1"
    Comme pour le problème précédent, écrire un code pour faire l'action une fois. Cette action comporte deux boucles, chacune pour aller tout à droite, ou pour revenir.
    
    Puis sélectionner tout ce code, et le décaler avec ++tab++ pour l'insérer dans un **bloc** de répétition, pour 20 tours.

??? tip "Indice 2"
    Modifier le code

    ```python
    from robot import *

    RÉPÉTER ??? FOIS
        ramasser...
        ...aller tout à droite...
        deposer...
        ...revenir tout à gauche...
    ```


## 10) Le Grand Évènement

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=643&iOrder=20) est de :

> Parcourir un carré $10×10$ et revenir au départ.

!!! tip "Indices"
    1. Commencer par préparer votre parcours sur papier.
    2. Étudier les étapes qui peuvent se répéter.
    3. Traduire votre idée en Python.

