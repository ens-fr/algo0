# 👾 France-IOI (I-6)

Le niveau 1 de France-IOI se poursuit avec [Structures avancées](http://www.france-ioi.org/algo/chapter.php?idChapter=647){target=_blank}

Des exercices un peu plus complexes, ici on ne donnera de l'aide que pour la lecture de l'entrée et l'affichage de la sortie.

!!! tip "Conseil général"
    N'oubliez pas de choisir de bons noms de variable et que votre code permette de comprendre quel était l'énoncé en le lisant.

    Dans le monde professionnel, le code est échangé et on travaille en équipe.

    1. Le code qu'on écrit doit être compris facilement par un collègue.
    2. **Mais aussi** par soi-même au bout de quelques temps.

## 1) Villes et villages

```python
nb_lieux = int(input())
...
RÉPÉTER ??? FOIS
    population = int(input())
    ...

print(nb_villes)
```



## 2) Planning de la journée

```python
position_actuelle = int(input())
nb_villages = int(input())
...
RÉPÉTER ??? FOIS
    position = int(input())
    ...
...
print(nb_villages_proches)
```

## 3) Étape la plus longue

```python
nb_jours = int(input())
...
RÉPÉTER ??? FOIS
    distance = int(input())
    ...

print(distance_maximale)
```

## 4) Calcul des dénivelées

```python
nb_variations = int(input())
...
RÉPÉTER ??? FOIS
    variation = int(input())  # positif ou négatif
    ...

print(cumul_montee)    # positif
print(cumul_descente)  # positif
```


## 5) Type d'arbres

Penser à faire un schéma !

```python
hauteur = int(input())
nb_folioles = int(input())
........

... print("Calaelen")
... print("Dorthonion")
... print("Falarion")
... print("Tinuviel")
```

## 6) Tarifs de l'auberge

Conseil : N'écrire qu'un seul `print` ; à la fin

```python
age = int(input())
poids_bagages = int(input())

...
print(prix)
```

## 7) Protection du village

```python
nb_maisons = int(input())
x_min = ...
x_max = ...
y_min = ...
y_max = ...
RÉPÉTER ??? FOIS
    x = int(input())
    y = int(input())
    ...

...
print(perimetre)
```


## 8) Le juste prix

```python
nb_marchands = int(input())
position = ...
prix_mini = ...
meilleure_position = ...
RÉPÉTER ??? FOIS
    prix = int(input())
    ...

print(meilleure_position)
```
