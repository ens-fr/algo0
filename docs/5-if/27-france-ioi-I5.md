# 👾 France-IOI (I-5)

Le niveau 1 de France-IOI se poursuit avec [Tests et conditions](http://www.france-ioi.org/algo/chapter.php?idChapter=646){target=_blank}

## 1) Transport des bagages

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&idTask=1988) est de :

> Afficher `Surcharge !` si c'est le cas après calcul.

!!! tip "Indice"
    Modifier le code

    ```python
    nb_paquets = int(input())
    poids_1_paquet = int(input())      # en kg
    ??? =                              # en kg
    ..........
        print("Surcharge !")
    ```


## 2) Bornes kilométriques

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=2) est de :

> Afficher l'écart entre deux nombres

!!! tip "Indices"
    - Pour calculer un écart, on calcule la différence entre le plus grand et le plus petit.
    - Utiliser une variable `ecart` que l'on affichera **uniquement** à la fin.


    Modifier le code

    ```python
    num_matin = int(input())
    num_soir = int(input())
    .......

    print(ecart)
    ```

## 3) Tarifs dégressifs

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=3) est de :

> Afficher le prix à payer en fonction de l'heure d'arrivée.

!!! tip "Indices"
    - Utiliser des variables `heure` et `prix` qui sera affiché à la fin.

    Modifier le code

    ```python
    heure = int(input())
    prix = ...
    ........

    print(prix)
    ```


## 4) Bagarre générale

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=4) est de :

> Afficher une information sous condition

!!! tip "Indices"
    On pourra faire deux tests, l'un après l'autre

    Modifier le code

    ```python
    superficie_Arignon = int(input())
    superficie_Evaran = int(input())
    .........
    print("La famille Arignon a un champ trop grand")
    .........
    print("La famille Evaran a un champ trop grand")
    ```

## 5) Tarif du bateau

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=6) est de :

> Donner un tarif en fonction de l'âge.

!!! tip "Indice"
    - Penser à `#!python else` au lieu de faire deux tests
    - Ne pas oublier `:` à sa suite...

    Modifier le code

    ```python
    age = int(input())

    .........
    print("Tarif réduit")
    .........
    print("Tarif plein")
    ```

## 6) Traversée du pont

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=9) est de :

> Afficher une taxe en fonction d'un lancer de deux dés.

!!! tip "Indices"
    Modifier le code

    ```python
    nombre_1 = int(input())
    nombre_2 = int(input())
    somme = ....
    ..........
    ```

## 7) Concours de tir à la corde

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=10) est de :

> Trouver le poids total de deux équipes et afficher celle qui a un avantage.

!!! tip "Indices"
    Il faut préparer une boucle de somme cumulée, pour deux équipes à la fois.

    Modifier le code

    ```python
    nb_membres = int(input())
    poids_total_1 = ...
    poids_total_1 = ...
    RÉPÉTER ??? FOIS
        poids_1 = int(input())
        poids_2 = int(input())
        ...
        ...
    .........
    print("L'équipe 1 a un avantage")
    ...
    print("L'équipe 2 a un avantage")
    ...
    print("Poids total pour l'équipe 1 :", poids_total_1)
    print("Poids total pour l'équipe 2 :", poids_total_2)
    ```


## 8) Mot de passe du village

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=646&iOrder=12) est de :

> Vérifier un mot de passe

!!! tip "Indices"
    1. Parmi les tests, il y a : `==`, `!=`, `<`, `>`, `<=`, `>=`
    2. Modifier le programme

    ```python
    code = int(input())
    ...
    print("Bon festin !")
    print("Allez-vous-en !")
    ```

