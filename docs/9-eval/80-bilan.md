# 🎓 Évaluation

!!! abstract "Sujet"
    Un fichier comporte des relevés, et on souhaite en avoir la moyenne.

    - une ligne avec un nombre entier de relevés : `nb_releves`
    - `nb_releves` lignes, avec chacune un relevé entier : `releve`

    Les relevés strictement négatifs sont considérés être des erreurs qu'il ne faut pas compter.
    
    On demande donc de calculer la moyenne des relevés positifs.

    - Si cette moyenne existe, il faut afficher la moyenne entière tronquée
    - S'il n'y a aucun relevé positif, il faut afficher `Pas de relevés`

!!! example "Exemple d'entrée"

    Il y a ici $4$ relevés, dont trois valides.

    ```email
    4
    1000
    2000
    -150
    6000
    ```

!!! success "Exemple de sortie"

    ```output
    3000
    ```

    En effet, la moyenne est $\dfrac{1000+2000+6000}3 = \dfrac{9000}3 = 3000$.

{{ IDE('bilan') }}
