# 👾 France-IOI (I-8)

Le niveau 1 de France-IOI se termine avec [Répétitions conditionnées](http://www.france-ioi.org/algo/chapter.php?idChapter=649){target=_blank}

On ne donnera de l'aide que pour la lecture de l'entrée et l'affichage de la sortie.


## 1) Département de médecine : contrôle d'une épidémie

```python
population = int(input())
...

print(jour)
```

## 2) Administration : comptes annuels

```python
entier = int(input())
...


print(somme)
```

## 3) Département de pédagogie : le « c'est plus, c'est moins »

```python
secret = int(input())
proposition = int(input())
nb_essais = ...
....


print("Nombre d'essais nécessaires :")
print(nb_essais)
```


## 4) Département d'architecture : construction d'une pyramide

!!! tip "Hauteur"
    Bien réfléchir à quoi correspond `hauteur`.

    - La hauteur déjà construite ?
    - La hauteur que vous envisagez ?

    Les deux sont possibles, mais le code est différent !

```python
nb_pierres_disponibles = int(input())
nb_pierres = ...
hauteur = ...

...
print(hauteur ???)
print(nb_pierres)
```


## 5) Département de chimie : mélange explosif

```python
nb_mesures = int(input())
temp_min = int(input())
temp_max = int(input())
temp_valide = ...         # un booléen

...temp = int(input())


...print("Rien à signaler")
...print("Alerte !!")
```


