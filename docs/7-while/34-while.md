# 🛗 Répétition conditionnée

On a vu comment répéter une action un certain nombre de fois. Par exemple, pour déplacer un robot de 10 cases à droite.

```python
from robot import *

for tour in range(10):
    droite()
```

Mais on aimerait pouvoir répéter une action ==**tant que**== une certaine condition est vraie.

On utilise alors avec Python une boucle `#!python while`

=== "Code"

    ```python
    while condition:
        ################
        #              #
        action()       #
        #              #
        ################
    suite()
    ```

=== "Pseudo code"

    ```{.email title="🧾 Algorithme"}
    RÉPÉTER TANT QUE LA condition EST VRAIE
        ################
        #              #
        action         #
        #              #
        ################
    suite
    ```


## Exemple

On voudrait compter le nombre de chiffres d'un entier.

=== "Exemple avec 8"

    ```email
    8
    ```

    ```python
    entier = int(input())
    nb_chiffres = 1
    while entier >= 10:
        entier = entier // 10
        nb_chiffres = nb_chiffres + 1
    print("L'entier entré avait", nb_chiffres, "chiffre(s).")
    ```

    ```output
    L'entier entré avait 1 chiffre(s).
    ```

    Ici, il n'y a **aucun** tour de boucle effectué, en effet `8 >= 10` est faux dès le début.

=== "Exemple avec 47"

    ```email
    47
    ```

    ```python
    entier = int(input())
    nb_chiffres = 1
    while entier >= 10:
        entier = entier // 10
        nb_chiffres = nb_chiffres + 1
    print("L'entier entré avait", nb_chiffres, "chiffre(s).")
    ```

    ```output
    L'entier entré avait 2 chiffre(s).
    ```

    Ici, il n'y a qu'**un seul** tour de boucle effectué, en effet
    
    * `47 >= 10` est vrai.
        - donc ensuite `entier = 47 // 10`, (`entier` sera égal à $4$)
        - et `nb_chiffres = 1 + 1`, (`nb_chiffres` sera égal à $2$)
    * `4 >= 10` est faux ; ==la boucle s'arrête==.

=== "Exemple avec 1337"

    ```email
    1337
    ```

    ```python
    entier = int(input())
    nb_chiffres = 1
    while entier >= 10:
        entier = entier // 10
        nb_chiffres = nb_chiffres + 1
    print("L'entier entré avait", nb_chiffres, "chiffre(s).")
    ```

    ```output
    L'entier entré avait 4 chiffre(s).
    ```

    Ici, il y a **3** tours de boucle effectués, en effet
    
    * `1337 >= 10` est vrai, puis
    * `133 >= 10` est vrai, puis
    * `13 >= 10` est vrai, puis
    * `1 >= 10` est faux ; ==la boucle s'arrête==.

    `nb_chiffres` valait $1$, et il a augmenté 3 fois de $1$, il termine à $4$ ; le résultat attendu.


## Boucle infinie

Le danger des répétitions conditionnées, c'est de faire une boucle qui ne s'arrête jamais...

```python
n = 0
while n >= 0:
    n = n + 1
print("Ce message ne sera jamais affiché")
```

Dans un mauvais programme, cette situation peut arriver. Il faut prévoir un mécanisme pour l'arrêter ! 

Sur France-IOI, il y a un mécanisme automatique qui arrête les programmes au bout d'un certain temps. On vous indique alors que soit votre programme est trop lent, soit il est rentré dans une boucle infinie. Ce sera à vous d'étudier votre code pour corriger le problème.
