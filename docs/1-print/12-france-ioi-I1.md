# 👾 France-IOI (I-1)

Le niveau 1 commence avec [Affichage de texte, suite d'instructions](http://www.france-ioi.org/algo/chapter.php?idChapter=642){target=_blank}

Voici quelques pistes pour commencer

## 1) Hello World!

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&idTask=1869) est :

Donner un programme qui affiche `Hello world!`

!!! tip "Indices"

    1. Vous pouvez utiliser le copier-coller pour le texte proposé
    2. Ne pas oublier les guillemets pour délimiter le texte
    3. La fonction qui affiche est `#!py print`
    4. Le code à compléter est :

    ```python
    print("...")
    ```

## 2) Présentation

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&iOrder=4) est :

- modifier le programme ci-dessous :

```python
print("Ma devise est 'Parler peu mais parler bien'.")
print("Je m'appelle Camthalion")
print("Coucou !")
```

- pour obtenir un affichage différent :

```output
Coucou !
Je m'appelle Camthalion
Ma devise est 'Parler peu mais parler bien'.
```

!!! tip "indices"

    1. Vous pouvez utiliser le copier-coller et utiliser la première version du programme.
    2. Vous pouvez utiliser le couper-coller pour déplacer les instructions.
    3. Pensez à lire **tous** les corrigés et **tous** les conseils donnés.

## 3) Plan de la montagne

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&iOrder=6) est :

- D'afficher le texte ci-dessous

```output
Tout droit tu grimperas,
La clé tu trouveras,
Habile tu seras,
Quand tu les porteras,
Et avec le chef tu reviendras !
```

!!! tip "Indices"

    1. Copier-coller le texte.
    2. Ajouter un `print` à chaque ligne.
    3. Penser aux parenthèses et aux guillemets pour chaque ligne.


## 4) Dans le fourré

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&iOrder=8) est :

- Déplacer le robot jusqu'à la clé.

Compléter le programme ci-dessous

```python
from robot import *  # À écrire (1)

haut()
haut()
...  # Compléter ici (2)
```

1. une seule fois, et **uniquement** pour les problèmes avec le robot de France-IOI.
2. en ajoutant 6 lignes.

!!! tip "Indices"

    1. On **ne peut pas** écrire `#!py haut(3)` pour aller en haut 3 fois.
    2. Il faut ajouter les instructions dans l'ordre.

## 5) Empilement de cylindres

> Ceci est un challenge, il vaut mieux le faire dans le calme, à la maison, et non en classe.

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&idTask=1873) est :

- Déplacer tous les disques de la zone 1 à la zone 3 en respectant les deux règles données.

Compléter le programme ci-dessous

```python
from robot import *

...  #1
deplacer(1, 3)
...  #3
```

??? tip "Indices"

    - Ne pas utiliser d'accents pour `#!py deplace()`
    - (#1) En plusieurs instructions, on peut déplacer trois disques dans la zone 2.
    - (#3) En plusieurs instructions, on peut déplacer, à nouveau, ces trois disques dans la zone 3. On le fait après avoir fait `#!py deplace(1, 3)` pour le plus gros disque.

    ![hanoi départ](images/hanoi-1.png){width=24%}
    ![hanoi milieu 1](images/hanoi-2.png){width=24%}
    ![hanoi milieu 2](images/hanoi-3.png){width=24%}
    ![hanoi fin](images/hanoi-4.png){width=24%}
    

## 6) Recette secrète

Le [problème](http://www.france-ioi.org/algo/task.php?idChapter=642&iOrder=12) est de :

- Remplir un tonneau de $4\,\text{L}$ avec des tonneaux de $3\,\text{L}$ et $5\,\text{L}$, ainsi qu'une fontaine.

Modifier le programme ci-dessous :

```python
from robot import *

remplir(5)
transferer(5, 3)
vider(5)
```

!!! tip "Indices"

    1. Faire un schéma en coordonnées cartésiennes.
        - $x$ la quantité dans le tonneau de $3\,\text{L}$.
        - $y$ la quantité dans le tonneau de $5\,\text{L}$.
    2. On part de $(0, 0)$ ; les deux tonneaux vides, $x=0$ et $y=0$.
    3. Essayer toutes les actions possibles et noter tout point $(x, y)$ que l'on peut atteindre.
    4. Objectif : arriver à $y=4$.
