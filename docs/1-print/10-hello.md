# 🗺️ Hello World!

## Une tradition

La tradition pour un premier programme informatique est d'afficher `Hello World!`

Avec Python, le langage que nous utiliserons.

{{ IDEv('hello') }}

- Pour lancer le script, appuyer sur : <button class="emoji">⚙️</button> 


## L'affichage avec Python

Avec `#!py print`, on peut afficher

-   du texte (à placer entre guillemets)
-   des nombres
-   d'autres objets...
-   ou rien qu'un saut de ligne

On peut afficher plusieurs objets à la suite avec un seul `print`. Il y a un saut de ligne à la fin.

!!! example "Nombres et texte"

    ```python
    print("Voici des nombres :", 123, 456,     789,   000000)
    print("Et du texte :",     "000"         )
    ```

    ```output
    Voici des nombres : 123 456 789 0
    Et du texte : 000
    ```
    
    -   À la fin de la première ligne, il n'y a qu'un `0` affiché, c'est le **nombre** $0$.
    -   La seconde ligne affiche le **texte** `000`, il était entre guillemets, Python ne l'a pas converti en nombre avant affichage. Ce texte est bien calé à gauche, placé **une** espace après celui qui précède.

!!! example "Espaces dans le code"

    ```python
    print("Si x =",5,"et y =", 8, ", alors x + y =", x + y)
    print("Simple",          "éloigné")
    ```

    ```output
    Si x = 5 et y = 8, alors x + y = 13
    Simple éloigné
    ```
    
    - Regarder les espaces dans le code, et dans le résultat.
    - Ça ne change rien, Python met **une** seule espace entre les objets affichés.

!!! warning "Bonne pratique"
    - On met une espace après la virgule dans le code.
    - On ne met pas d'espace avant la virgule.

    **Une** bonne façon d'écrire ce code est donc :

    ```python
    print("Si x =", 5, "et y =", 8, ", alors x + y =", x + y)
    print("Simple", "éloigné")
    ```


!!! info "Deux choses à signaler"

    1.  `#!py print` sépare les objets avec **une** seule espace.
    2.  `#!py print` termine par un saut de ligne à la fin.


!!! danger "On peut changer ce comportement"

    - `sep` : pour _separator_, permet de choisir le séparateur entre les éléments
        - par défaut : `#!py sep=" "` une espace
    - `end` : pour la fin, permet de choisir la fin de l'affichage
        - par défaut : `#!py end="\n"` un saut de ligne
    
    ```python
    print("mot", 12345, "colle", 7, sep="", end="")
    print("fin")
    ```

    ```output
    mot12345colle7fin
    ```

    1.  Le premier `#!py print` a affiché chaque objet en les séparant (`sep`) avec du vide `""` (au lieu de l'espace), et a fini (`end`) avec du vide (au lieu du saut de ligne).
    2.  Le second `print` enchaine donc avec `fin` collé. Mais il termine avec un saut de ligne, lui !

### Exercice 1

Deviner l'affichage produit par ce programme

```python
print("abc", 123, "567", 8, sep="...", end="===")
print(42, 1337, sep=" T ")
```

=== "Votre réponse"
    
    - [ ] `"abc 123 567 8 sep ... ==="`
    - [ ] `42T1337`
    - [ ] `abc...123...567...8===42 T 1337`
    - [ ] `"abc"...123..."567"...8===`

=== "Solution"

    - ❌ `"abc 123 567 8 sep ... ==="`
    - ❌ `42T1337`
    - ✅ `abc...123...567...8===42 T 1337`
    - ❌ `"abc"...123..."567"...8===`

    Justification :

    - Le premier affichage produit `abc...123...567...8===` **mais** ne saute pas à la ligne.
    - Le second affichage produit `42 T 1337`, puis saute à la ligne.

### Exercice 2

{{ IDEv('affiche') }}

??? success "Réponse"

    ```python
    print(0, 1, 1, 2, 3, 5, 8, 13, 21, sep=" | ")
    ```

    On a juste ajouté `, sep=" | "`

!!! info "Utilisation dans le développement web"
    Cette technique est utilisée pour créer des pages web dynamiquement.
    On écrit un programme qui produit automatiquement du code HTML en fonction de données dans un tableau. On peut aussi créer du Markdown ou tout autre sortie qui est envoyée à un autre programme.

    Bienvenue dans le monde de la programmation ! :smile:

## L'essentiel

!!! savoir "À retenir"
    1.  Pour afficher du texte **et** sauter à la ligne, on peut faire :
      
        ```python
        print("Voici un message")
        ```

        ```output
        Voici un message
        ```
    2.  Pour afficher un collage de morceaux de texte, on peut faire :
      
        ```python
        print("début", end="")
        print("suite1", end="")
        print("suite2", end="")
        print("fin")
        ```

        ```output
        débutsuite1suite2fin
        ```
        Cette technique permet de construire une ligne en plusieurs étapes.
    3.  Pour afficher une ligne vide, il suffit de faire
        
        ```python
        print("")
        ```

        ou alors

        ```python
        print()
        ```
